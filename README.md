# shiki-rn

# Requirements
- ruby 3.0.2
- node 16.7.0
- yarn 1.22.11
- bundler 2.2.22
- watchman

# FAQ
1. Error: Could not find node. Make sure it is in bash PATH or set the NODE_BINARY environment variable. Command PhaseScriptExecution failed with a nonzero exit code
   
   `sudo ln -s $(which node) /usr/local/bin/node`
