import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  Button,
  // useColorScheme,
  // View,
} from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import first from '@monorepo/package1';
import second from '@monorepo/package2';
import SegmentedControl from '@monorepo/package1/SegmentedControl';

const HomeScreen = () => {
  const navigation = useNavigation();
  const [selectedIndex, setSelectedIndex] = useState(0);
  return (
    <SafeAreaView style={{ backgroundColor: 'white' }}>
      <StatusBar barStyle={'dark-content'} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Text>
          {first()}, {second()}!
        </Text>
        <SegmentedControl
          values={['One', 'Two']}
          selectedIndex={selectedIndex}
          onChange={(e) => setSelectedIndex(e.nativeEvent.selectedSegmentIndex)}
        />
        <Button
          onPress={() => navigation.navigate('Modal')}
          title="Open Modal"
        />
        <Button
          onPress={() => {
            console.log('Profile');
            navigation.navigate('Profile');
          }}
          title="Open profile"
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const ProfileScreen = () => (
  <Text>Profile</Text>
);

const ModalNav = createNativeStackNavigator();
const Modals = () => (
  <ModalNav.Navigator>
    <ModalNav.Screen name={'InitialModal'} component={ModalScreen} />
    <ModalNav.Screen name={'PushModal'} component={PushModalScreen} />
  </ModalNav.Navigator>
);

const ModalScreen = () => {
  const navigation = useNavigation();
  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => <Button title={'Dismiss'} onPress={navigation.goBack} />
    });
  }, [navigation]);
  return (
  <>
    <Text>Modal</Text>
    <Button
      onPress={() => navigation.push('PushModal')}
      title="PushModal"
    />
  </>
  );
};

const PushModalScreen = () => {
  const navigation = useNavigation();
  return (
  <>
    <Text>PushModal</Text>
    <Button
      onPress={() => navigation.pop()}
      title="Pop"
    />
  </>
  );
};

const TabNav = createBottomTabNavigator();
const TabBar = () => (
  <TabNav.Navigator>
    <TabNav.Screen name={'Home'} component={HomeScreen} />
    <TabNav.Screen name={'Profile'} component={ProfileScreen} />
  </TabNav.Navigator>
);

const MainNav = createNativeStackNavigator();
const App: React.FC<never> = () => (
  <NavigationContainer>
    <MainNav.Navigator>
      <MainNav.Screen name={'TabBar'} component={TabBar} options={{ headerShown: false }} />
      <MainNav.Screen name={'Modal'} component={Modals} options={{ presentation: 'modal' , headerShown: false }} />
    </MainNav.Navigator>
  </NavigationContainer>
);

export default App;
